unit udisable;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Buttons, ExtCtrls,
  StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    btnToggle: TButton;
    btnSel: TButton;
    btnCheck: TButton;
    btnSave: TButton;
    Edit1: TEdit;
    OpenDialog1: TOpenDialog;
    procedure btnCheckClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnToggleClick(Sender: TObject);
    procedure btnSelClick(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;
  FileName: string;
  aFile: THandle;
  FileAttr: LongInt;
  FileHeader: array [0..127] of LongWord;
  aCount: word;

const
  FlagsPos = 6; // 4 bytes from byte $18 = LongWord 6
  FlagsVal = $00200085; // Enable ALSR


implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnSelClick(Sender: TObject);
begin
 if OpenDialog1.Execute then begin
   FileName:= OpenDialog1.FileName;
   aFile := FileOpen(FileName,fmOpenReadWrite);
   aCount:= FileRead(aFile,FileHeader,64);
   if aCount > 32 then begin
     btnSel.Hide;
     btnCheck.Show;
     btnToggle.Show;
   end
   else begin
     Edit1.Text:= 'Fail';
     FileClose(aFile);
   end;
 end;
end;

procedure TForm1.btnCheckClick(Sender: TObject);
begin
  Edit1.Text:= IntToHex(FileHeader[FlagsPos]);
end;

procedure TForm1.btnSaveClick(Sender: TObject);
begin
 FileSeek(aFile,0,0);
 FileWrite(aFile,FileHeader,aCount);
 FileClose(aFile);
 btnSel.Show;
 btnToggle.Hide;
 btnCheck.Hide;
 btnSave.Hide;
 Edit1.Text:= 'Done';
end;

procedure TForm1.btnToggleClick(Sender: TObject);
begin
  if FileHeader[FlagsPos] = 0 then FileHeader[FlagsPos] := FlagsVal
  else FileHeader[FlagsPos] := 0;
  btnSave.Show;
end;

end.

